## 2.0.2
- updated version constraint fast_immutable_collections to '>=9.0.0 <11.0.0'

## 2.0.1
- Fixed README image not displayed correctly

## 2.0.0
- **BREAKING** Updated library exports to better fit the use-cases
- **BREAKING** NoticeRecord.stackTrace is no longer nullable
- **BREAKING** NoticeRecord.messageObject is gone and replaced by NoticeRecord.message
- ConsoleOutput is now a shortcut for a WriterOutput of a Simple- or PrettyFormatter
- More readable stack traces using the stack_trace package
- Added NoticeFormatter interface for modular record-to-string formatting components
- Added PrettyFormatter for console readability improvements, the new default of ConsoleOutput

## 1.2.1
- Fixed table in the readme

## 1.2.0
- Added messageObject field to NoticeRecord
- StackTraces will now be set automatically when not given by the user

## 1.1.0
- Added version constant

## 1.0.4
- Fixed dart formatting problems to pass static analysis

## 1.0.3
- Added analysis stage

## 1.0.2
- Added tests

## 1.0.1
- Added topics to pubspec yaml

## 1.0.0
- First release version

## 1.0.0-alpha.9
- Updated readme

## 1.0.0-alpha.8
- Added badges

## 1.0.0-alpha.7
- Updated description

## 1.0.0-alpha.6
- Ran dart format

## 1.0.0-alpha.5
- Added most of the documentation

## 1.0.0-alpha.4
- Some cleanup and minor changes

## 1.0.0-alpha.3
- Downgraded meta to ^1.10.0 for better flutter compatability

## 1.0.0-alpha.2
- Removed unnecessary dependecy package logger

## 1.0.0-alpha.1
- Initial version.
