# Notice

[![Pub Points](https://img.shields.io/pub/points/notice)](https://pub.dev/packages/notice)
[![Pub Version](https://img.shields.io/pub/v/notice)](https://pub.dev/packages/notice)
[![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/happy-square%2Fnotice)](https://gitlab.com/happy-square/notice/-/pipelines)
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/happy-square%2Fnotice)](https://gitlab.com/happy-square/notice)
[![GitLab (self-managed)](https://img.shields.io/gitlab/license/happy-square%2Fnotice)](https://gitlab.com/happy-square/notice)

Notice is an extensible logger with an intuitive tree-like structure. Notice excels in seamless cross-package logging while offering a simple yet robust API for developers.

### Simplest example
```dart
final notice = Notice(
  outputs: [ConsoleOutput()],
);
notice.info("Logging is fun!");
notice.error("An error accoured", error: "Example error");
```
Running this code will give you
![Example Output](https://gitlab.com/happy-square/notice/-/raw/main/img/example_output.png?ref_type=heads)

### Creating sub-loggers
Always know where the message came from using breadcrumbs
```dart
final notice = Notice(outputs: [ConsoleOutput.simple()], breadcrumb: "main");
final subNotice = Notice.childOf(notice, breadcrumb: "foo");
subNotice.info("Sub Logger message");
```
prints:\
`2024-01-18T14:47:30.117487 main.foo INFO: Sub Logger message`

### Filtering messages
If you need multiple filters you can also use `CombinedFilter`
```dart
final notice = Notice(
  outputs: [
    FilteredOutput(
      LevelFilter(NoticeLevel.info),
      ConsoleOutput.simple(),
    ),
  ],
);
notice.info("Will be logged");
notice.warn("Will also be logged");
notice.trace("Will not be logged");
```
prints:\
`2024-01-18T14:47:30.716747 INFO: Will be logged`\
`2024-01-18T14:47:30.721771 WARN: Will also be logged`

### Third-Party-Package logging
```dart
Notice(
  outputs: [ConsoleOutput.simple()],
  registries: [globalNoticeRegistry],
);
final thirdPartyPackageNotice = Notice(
  parent: globalNoticeRegistry,
  breadcrumb: "BarPackage",
);
thirdPartyPackageNotice.error("Error from another package");
```
prints:\
`2024-01-18T14:47:31.319375 BarPackage ERROR: Error from another package`

## Extensible
Create your own implementation of filters and outputs to extend the behaviour of this package

|package |description |
|:---|:---|
|[notice_sentry](https://pub.dev/packages/notice_sentry)|Notice integration for sentry|

## Roadmap
- **Flutter library for loggin using dart:developer**\
  To increase readability when logging from a flutter app
  This could also make a good opportunity for adding debug widgets
- **Adapters to most of the common dart loggers**\
  To improve the experience when switching to notice and not make package authors clients dependent on it
