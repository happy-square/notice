# Notice

[![Pub Points](https://img.shields.io/pub/points/notice)](https://pub.dev/packages/notice)
[![Pub Version](https://img.shields.io/pub/v/notice)](https://pub.dev/packages/notice)
[![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/happy-square%2Fnotice)](https://gitlab.com/happy-square/notice/-/pipelines)
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/happy-square%2Fnotice)](https://gitlab.com/happy-square/notice)
[![GitLab (self-managed)](https://img.shields.io/gitlab/license/happy-square%2Fnotice)](https://gitlab.com/happy-square/notice)

Notice is an extensible logger with an intuitive tree-like structure. Notice excels in seamless cross-package logging while offering a simple yet robust API for developers.

### Simplest example
::[EXAMPLE; name: 'simple_example', output: false]
Running this code will give you
![Example Output](img/example_output.png)

### Creating sub-loggers
Always know where the message came from using breadcrumbs
::[EXAMPLE; name: 'sub_logger_example', output: true]

### Filtering messages
If you need multiple filters you can also use `CombinedFilter`
::[EXAMPLE; name: 'filter_example', output: true]

### Third-Party-Package logging
::[EXAMPLE; name: 'third_party_package_example', output: true]

## Extensible
Create your own implementation of filters and outputs to extend the behaviour of this package

|package |description |
|:---|:---|
|[notice_sentry](https://pub.dev/packages/notice_sentry)|Notice integration for sentry|

## Roadmap
- **Flutter library for loggin using dart:developer**\
  To increase readability when logging from a flutter app
  This could also make a good opportunity for adding debug widgets
- **Adapters to most of the common dart loggers**\
  To improve the experience when switching to notice and not make package authors clients dependent on it
