import 'package:notice/filters.dart';
import 'package:notice/notice.dart';
import 'package:notice/outputs.dart';

void main() {
  final notice = Notice(
    outputs: [
      FilteredOutput(
        LevelFilter(NoticeLevel.info),
        ConsoleOutput.simple(),
      ),
    ],
  );
  notice.info("Will be logged");
  notice.warn("Will also be logged");
  notice.trace("Will not be logged");
}
