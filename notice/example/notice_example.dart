import 'package:notice/filters.dart';
import 'package:notice/notice.dart';
import 'package:notice/outputs.dart';
import 'package:notice/registry.dart';

void main() {
  final notice = Notice(
    outputs: [
      FilteredOutput(
        LevelFilter(NoticeLevel.info),
        ConsoleOutput.simple(),
      ),
    ],
    registries: [globalNoticeRegistry],
  );
  notice.info("Hello World");
  notice.trace("Will not be logged");

  final subNotice = Notice(parent: notice);
  subNotice.info("Hello World pt. 2");

  final thirdPartyNotice =
      Notice(parent: globalNoticeRegistry, breadcrumb: "FooPackage");
  thirdPartyNotice.error("Hello World from another package");

  final thirdPartySubPackage =
      Notice(parent: thirdPartyNotice, breadcrumb: "BarProcessing");
  thirdPartySubPackage.warn("Sub log from another package");
}
