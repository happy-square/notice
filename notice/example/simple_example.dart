import 'package:notice/notice.dart';
import 'package:notice/outputs.dart';

void main() {
  final notice = Notice(
    outputs: [ConsoleOutput()],
  );
  notice.info("Logging is fun!");
  notice.error("An error accoured", error: "Example error");
}
