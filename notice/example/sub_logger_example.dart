import 'package:notice/notice.dart';
import 'package:notice/outputs.dart';

void main() {
  final notice = Notice(outputs: [ConsoleOutput.simple()], breadcrumb: "main");
  final subNotice = Notice.childOf(notice, breadcrumb: "foo");
  subNotice.info("Sub Logger message");
}
