import 'package:notice/notice.dart';
import 'package:notice/outputs.dart';
import 'package:notice/registry.dart';

void main() {
  Notice(
    outputs: [ConsoleOutput.simple()],
    registries: [globalNoticeRegistry],
  );
  final thirdPartyPackageNotice = Notice(
    parent: globalNoticeRegistry,
    breadcrumb: "BarPackage",
  );
  thirdPartyPackageNotice.error("Error from another package");
}
