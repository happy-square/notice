import 'package:notice/src/notice_registry.dart';

export 'src/notice_registry.dart';

/// The global notice registry, where packages typically register their root notices.
final globalNoticeRegistry = NoticeRegistry();
