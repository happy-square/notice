import 'package:notice/filter.dart';

/// A filter that combines multiple filters.
class CombinedFilter implements NoticeFilter {
  /// Creates a new combined filter.
  const CombinedFilter(this.filters);

  /// The filters to combine.
  final List<NoticeFilter> filters;

  @override
  bool filter(NoticeRecord record) {
    return filters.every((filter) => filter.filter(record));
  }
}
