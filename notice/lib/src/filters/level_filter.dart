import 'package:notice/filter.dart';
import 'package:notice/notice.dart';

/// A filter that filters notices by their level.
class LevelFilter implements NoticeFilter {
  /// Creates a new level filter.
  const LevelFilter(this.level);

  /// The level to filter by.
  /// Only notices with a level greater than or equal the given level will be passed.
  final NoticeLevel level;

  @override
  bool filter(NoticeRecord record) {
    return record.level.priority >= level.priority;
  }
}
