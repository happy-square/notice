import 'dart:convert';

import 'package:ansicolor/ansicolor.dart';
import 'package:meta/meta.dart';
import 'package:notice/formatter.dart';
import 'package:notice/notice.dart';
import 'package:notice/src/notice_writer_context.dart';
import 'package:notice/src/tools/ansi_pen_colors_extension.dart';
import 'package:notice/src/tools/level_map_select_extension.dart';
import 'package:notice/src/tools/unicode_border_style.dart';
import 'package:stack_trace/stack_trace.dart';

final levelToAnsiPenMap = {
  NoticeLevel.fatal: AnsiPen()..red(),
  NoticeLevel.error: AnsiPen()..red(),
  NoticeLevel.warn: AnsiPen()..orange(),
  NoticeLevel.info: AnsiPen()..blue(),
  NoticeLevel.debug: AnsiPen()..gray(level: 0.5),
  NoticeLevel.trace: AnsiPen()..gray(level: 0.5),
};

class PrettyFormatter implements NoticeFormatter {
  /// Creates a new [PrettyFormatter].
  /// The pretty formatter is focussed on increasing log readability
  const PrettyFormatter({
    this.colors = true,
    this.lineLength = 80,
    this.borderStyle = UnicodeBorderStyle.light,
    this.jsonIndent = 2,
    this.terseStackTracs = true,
  });

  /// Whether to use colors in the output.
  final bool colors;

  /// The length of the output lines.
  final int lineLength;

  /// The style of the border
  final UnicodeBorderStyle borderStyle;

  /// The indent when the messages are formatted as json
  final int jsonIndent;

  /// Whether to use terse stack traces.
  /// Terse stack traces remove frames from core dart libraries
  /// to be more readable.
  final bool terseStackTracs;

  @override
  String format(NoticeRecord record, NoticeWriterContext context) {
    final parts = [
      createBody(record),
      createError(record),
      createStackTrace(record),
    ].whereType<String>().toList();
    final borderPen = context.colorSupport
        ? levelToAnsiPenMap.getByEqualLevelOrHigher(record.level)
        : AnsiPen();
    return createBox(createHeader(record), parts, borderPen);
  }

  @visibleForTesting
  String createHeader(NoticeRecord record) {
    final buffer = StringBuffer();
    buffer.write(record.level.name);
    buffer.write(" ");
    if (record.breadcrumbs.isNotEmpty) {
      buffer.write(record.breadcrumbs.join('.'));
      buffer.write(" ");
    }
    buffer.write(record.timestamp.toIso8601String().replaceFirst('T', ' '));
    return buffer.toString();
  }

  String? createStackTrace(NoticeRecord record) {
    if (record.error == null) return null;
    var trace = Trace.from(record.stackTrace);
    if (terseStackTracs) {
      trace = trace.terse;
    }
    return trace.terse.toString().trim();
  }

  String? createError(NoticeRecord record) {
    return record.error?.toString();
  }

  @visibleForTesting
  String createBody(NoticeRecord record) {
    final message = record.message;
    if (message is Iterable || message is Map) {
      final encoder =
          JsonEncoder.withIndent('  ' * jsonIndent, (v) => v.toString());
      return encoder.convert(message);
    }
    return record.message.toString();
  }

  @visibleForTesting
  String createBox(String header, List<String> parts, AnsiPen borderPen) {
    final buffer = StringBuffer();
    buffer.write(borderPen(borderStyle.downAndRight +
        borderStyle.horizontal +
        header.replaceAll(' ', borderStyle.horizontal) +
        borderStyle.horizontal * (lineLength - header.length - 3) +
        borderStyle.downAndLeft));
    buffer.writeln();
    for (final (index, part) in parts.indexed) {
      for (final line in part.split('\n')) {
        final vertical = borderPen(borderStyle.vertical);
        buffer.write(vertical);
        buffer.write(' ');
        if (line.length <= lineLength - 2) {
          buffer.write(line.padRight(lineLength - 3));
          buffer.write(vertical);
        } else {
          buffer.write(line);
        }
        buffer.writeln();
      }
      if (index < parts.length - 1) {
        buffer.writeln(borderPen(borderStyle.verticalAndRight +
            borderStyle.horizontal * (lineLength - 2) +
            borderStyle.verticalAndLeft));
      }
    }
    buffer.write(borderPen(borderStyle.upAndRight +
        borderStyle.horizontal * (lineLength - 2) +
        borderStyle.upAndLeft));
    return buffer.toString();
  }
}
