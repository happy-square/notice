import 'package:notice/src/notice_formatter.dart';
import 'package:notice/src/notice_record.dart';
import 'package:notice/src/notice_writer_context.dart';

/// A simple formatter that formats a [NoticeRecord] into a single line.
class SimpleFormatter implements NoticeFormatter {
  /// Creates a new [SimpleFormatter].
  const SimpleFormatter();

  @override
  String format(NoticeRecord record, NoticeWriterContext writerContext) {
    final output = StringBuffer();
    output.write(record.timestamp.toIso8601String());
    if (record.breadcrumbs.isNotEmpty) {
      output.write(' ');
      output.write(record.breadcrumbs.join('.'));
    }
    output.write(' ');
    output.write(record.level);
    output.write(': ');
    output.write(record.message);
    if (record.error != null) {
      output.writeln();
      output.write(record.error);
      output.writeln();
      output.write(record.stackTrace);
    }
    return output.toString();
  }
}
