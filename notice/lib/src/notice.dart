import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:meta/meta.dart';
import 'package:notice/registry.dart';
import 'package:notice/src/notice_logger.dart';
import 'package:notice/src/notice_output.dart';
import 'package:notice/src/notice_reciever.dart';
import 'package:notice/src/notice_record.dart';

/// A notice can receive logs and represents a node in the notice tree structure.
/// Typically the root notice is responsible for outputting the logs,
/// while the children take the role of grouping logs together.
/// Each notice must have only one parent, but can have multiple children.
class Notice with NoticeLogger implements NoticeReceiver {
  /// Creates a new notice.
  Notice({
    this.parent,

    /// The outputs that will be used to output the logs.
    List<NoticeOutput> outputs = const [],
    this.breadcrumb,

    /// The registries that will be connected to this notice.
    List<NoticeRegistry> registries = const [],
  }) : outputs = outputs.toIList() {
    for (final registry in registries) {
      registry.connect(this);
    }
  }

  /// Creates a const sub notice that will simply log to the parent notice
  const Notice.childOf(this.parent, {this.breadcrumb})
      : outputs = const IListConst([]);

  /// The parent notice, if any.
  final NoticeReceiver? parent;

  /// The outputs that will be used to output the logs.
  final IList<NoticeOutput> outputs;

  /// The breadcrumb that will be added to the logs to better identify the source of the log.
  final String? breadcrumb;

  @override
  @internal
  void onRecord(NoticeRecord record) {
    if (breadcrumb != null) {
      record = record.copyWithBreadcrumb(breadcrumb!);
    }
    parent?.onRecord(record);
    _outputRecord(record);
  }

  void _outputRecord(NoticeRecord record) {
    for (final output in outputs) {
      output.onRecord(record);
    }
  }
}
