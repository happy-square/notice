import 'package:notice/src/notice_record.dart';

/// An interface class that allows to filter notices.
abstract interface class NoticeFilter {
  /// Returns `false` if the [record] should be filtered out.
  bool filter(NoticeRecord record);
}
