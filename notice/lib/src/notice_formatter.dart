import 'package:notice/filter.dart';
import 'package:notice/src/notice_writer_context.dart';

/// A formatter that formats [NoticeRecord]s into strings.
abstract interface class NoticeFormatter {
  /// Formats the [record] into a string.
  String format(NoticeRecord record, NoticeWriterContext writerContext);
}
