/// A level of a notice log.
/// In addition to the levels defined here, you can also define your own levels.
class NoticeLevel {
  /// Creates a new notice level.
  /// Take a look at the predefined levels before creating your own.
  const NoticeLevel(this.priority, this.name);

  /// The name of this level.
  /// The name will be used in the log output.
  final String name;

  /// The priority of this level.
  final int priority;

  static const trace = NoticeLevel(1000, 'TRACE');
  static const debug = NoticeLevel(2000, 'DEBUG');
  static const info = NoticeLevel(3000, 'INFO');
  static const warn = NoticeLevel(4000, 'WARN');
  static const error = NoticeLevel(5000, 'ERROR');
  static const fatal = NoticeLevel(6000, 'FATAL');

  @override
  operator ==(Object other) {
    if (other is NoticeLevel) {
      return priority == other.priority;
    } else {
      return false;
    }
  }

  @override
  int get hashCode => priority.hashCode;

  @override
  String toString() => name;
}
