import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:meta/meta.dart';
import 'package:notice/notice.dart';
import 'package:notice/src/notice_reciever.dart';
import 'package:notice/src/notice_record.dart';
import 'package:stack_trace/stack_trace.dart';

/// A mixin that adds logging methods to a [NoticeReceiver].
@internal
mixin NoticeLogger implements NoticeReceiver {
  /// Logs a record to the notice tree.
  void log(NoticeLevel level, Object? message,
      {Object? error, StackTrace? stackTrace}) {
    logInternal(
        level: level, message: message, error: error, stackTrace: stackTrace);
  }

  /// Logs a trace level record to the notice tree.
  void trace(Object? message, {Object? error, StackTrace? stackTrace}) {
    logInternal(
        level: NoticeLevel.trace,
        message: message,
        error: error,
        stackTrace: stackTrace);
  }

  /// Logs a debug level record to the notice tree.
  void debug(Object? message, {Object? error, StackTrace? stackTrace}) {
    logInternal(
        level: NoticeLevel.debug,
        message: message,
        error: error,
        stackTrace: stackTrace);
  }

  /// Logs a info level record to the notice tree.
  void info(Object? message, {Object? error, StackTrace? stackTrace}) {
    logInternal(
        level: NoticeLevel.info,
        message: message,
        error: error,
        stackTrace: stackTrace);
  }

  /// Logs a warn level record to the notice tree.
  void warn(Object? message, {Object? error, StackTrace? stackTrace}) {
    logInternal(
        level: NoticeLevel.warn,
        message: message,
        error: error,
        stackTrace: stackTrace);
  }

  /// Logs a error level record to the notice tree.
  void error(Object? message, {Object? error, StackTrace? stackTrace}) {
    logInternal(
        level: NoticeLevel.error,
        message: message,
        error: error,
        stackTrace: stackTrace);
  }

  /// Logs a fatal level record to the notice tree.
  void fatal(Object? message, {Object? error, StackTrace? stackTrace}) {
    logInternal(
        level: NoticeLevel.fatal,
        message: message,
        error: error,
        stackTrace: stackTrace);
  }

  @visibleForTesting
  @internal
  logInternal({
    required NoticeLevel level,
    required Object? message,
    required Object? error,
    required StackTrace? stackTrace,
  }) {
    final effectiveStackTrace = stackTrace ?? Trace.current(2);
    final String effectiveMessage;

    if (message is String) {
      effectiveMessage = message;
    } else {
      effectiveMessage = message.toString();
    }

    final record = NoticeRecord(
      level: level,
      message: effectiveMessage,
      error: error,
      stackTrace: effectiveStackTrace,
      timestamp: DateTime.now(),
      breadcrumbs: const IListConst([]),
    );
    onRecord(record);
  }
}
