import 'package:notice/src/notice_reciever.dart';

/// An interface class that allows to output notices to a specific destination.
abstract interface class NoticeOutput implements NoticeReceiver {}
