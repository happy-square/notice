import 'package:meta/meta.dart';

import 'package:notice/src/notice_record.dart';

/// An internal interface class that allowes to receive notices.
@internal
abstract interface class NoticeReceiver {
  /// Called when a notice is recorded.
  void onRecord(NoticeRecord record);
}
