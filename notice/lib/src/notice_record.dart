import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:meta/meta.dart';
import 'package:notice/src/notice_level.dart';

/// A record of a notice log.
class NoticeRecord {
  const NoticeRecord({
    required this.level,
    required this.message,
    required this.error,
    required this.stackTrace,
    required this.timestamp,
    required this.breadcrumbs,
  });

  /// The level of the log.
  final NoticeLevel level;

  /// The message of the log.
  final Object? message;

  /// The error of the log, if any.
  final Object? error;

  /// The stack trace of the log, if any.
  final StackTrace stackTrace;

  /// The timestamp of the log.
  final DateTime timestamp;

  /// The collected breadcrumbs of the log.
  /// The breadcrumbs are ordered in increasing specificity.
  /// The first breadcrumb is the most general, and the last is the most specific.
  final IList<String> breadcrumbs;

  @internal
  NoticeRecord copyWithBreadcrumb(String breadcrumb) {
    return NoticeRecord(
      level: level,
      message: message,
      error: error,
      stackTrace: stackTrace,
      timestamp: timestamp,
      // append breadcrumb to the front of the list
      breadcrumbs: breadcrumbs.insert(0, breadcrumb),
    );
  }

  @override
  String toString() {
    return 'NoticeRecord(level: $level, message: $message, error: $error, stackTrace: $stackTrace, timestamp: $timestamp, breadcrumbs: $breadcrumbs)';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NoticeRecord &&
          runtimeType == other.runtimeType &&
          level == other.level &&
          message == other.message &&
          error == other.error &&
          stackTrace == other.stackTrace &&
          timestamp == other.timestamp &&
          breadcrumbs == other.breadcrumbs;

  @override
  int get hashCode =>
      level.hashCode ^
      message.hashCode ^
      error.hashCode ^
      stackTrace.hashCode ^
      timestamp.hashCode ^
      breadcrumbs.hashCode;
}
