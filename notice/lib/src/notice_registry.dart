import 'package:meta/meta.dart';
import 'package:notice/src/notice_reciever.dart';
import 'package:notice/src/notice_record.dart';

/// Allows registering one or more notices without knowing their parent yet.
/// [globalNoticeRegistry] is a global instance of this class and is typically used to register
/// package level notices allowing to defer their output behavior to the package user.
///
/// Disclaimer:
/// Avoid using the registry where you can. It keeps references to receiving notices and prevents
/// them from being garbage collected.
class NoticeRegistry implements NoticeReceiver {
  final List<NoticeReceiver> _receivers = List.empty(growable: true);

  /// Creates a new notice registry.
  NoticeRegistry();

  @internal
  void connect(NoticeReceiver notice) {
    _receivers.add(notice);
  }

  @override
  @internal
  void onRecord(NoticeRecord record) {
    for (final receiver in _receivers) {
      receiver.onRecord(record);
    }
  }
}
