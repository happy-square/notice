import 'package:notice/src/notice_writer_context.dart';

/// A writer that writes strings.
abstract interface class NoticeWriter {
  /// Returns the [NoticeWriterContext] for this writer.
  NoticeWriterContext context();

  /// Writes the [string].
  void write(String string);
}
