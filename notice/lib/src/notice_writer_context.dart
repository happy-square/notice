/// Context for [NoticeWriter].
/// Holds information about the capabilities of the writer.
class NoticeWriterContext {
  const NoticeWriterContext({
    required this.colorSupport,
  });

  /// Whether the writer supports color.
  final bool colorSupport;
}
