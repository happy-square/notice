import 'package:notice/formatter.dart';
import 'package:notice/formatters.dart';
import 'package:notice/output.dart';
import 'package:notice/outputs.dart';
import 'package:notice/writers.dart';

/// A shortcut for creating a [WriterOutput] that writes to the console.
class ConsoleOutput implements NoticeOutput {
  /// Creates a new [ConsoleOutput].
  const ConsoleOutput({
    this.formatter = const PrettyFormatter(),
  });

  /// Creates a new [ConsoleOutput] with a [SimpleFormatter].
  /// This is a shortcut for `ConsoleOutput(formatter: SimpleFormatter())`.
  const ConsoleOutput.simple() : this(formatter: const SimpleFormatter());

  /// Creates a new [ConsoleOutput] with a [PrettyFormatter].
  /// This is a shortcut for `ConsoleOutput(formatter: PrettyFormatter())`.
  /// This is also the default.
  const ConsoleOutput.pretty() : this(formatter: const PrettyFormatter());

  final NoticeFormatter formatter;

  NoticeOutput get _output => WriterOutput(
        formatter: formatter,
        writer: const ConsoleWriter(),
      );

  @override
  void onRecord(NoticeRecord record) => _output.onRecord(record);
}
