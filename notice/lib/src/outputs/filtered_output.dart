import 'package:notice/src/notice_filter.dart';
import 'package:notice/src/notice_output.dart';
import 'package:notice/src/notice_record.dart';

/// An output that filters notices before passing them to another output.
class FilteredOutput implements NoticeOutput {
  /// Creates a new filtered output.
  const FilteredOutput(this.filter, this.output);

  /// The filter to use.
  final NoticeFilter filter;

  /// The output to pass the notices to.
  final NoticeOutput output;

  @override
  void onRecord(NoticeRecord record) {
    if (filter.filter(record)) {
      output.onRecord(record);
    }
  }
}
