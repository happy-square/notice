import 'package:notice/formatter.dart';
import 'package:notice/output.dart';
import 'package:notice/writer.dart';

/// An [NoticeOutput] that writes to a [NoticeWriter].
/// Strings are formatted using a [NoticeFormatter].
class WriterOutput implements NoticeOutput {
  const WriterOutput({
    required this.formatter,
    required this.writer,
  });

  final NoticeFormatter formatter;
  final NoticeWriter writer;

  @override
  void onRecord(NoticeRecord record) {
    final context = writer.context();
    final string = formatter.format(record, context);
    writer.write(string);
  }
}
