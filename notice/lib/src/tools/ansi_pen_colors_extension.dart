import 'package:ansicolor/ansicolor.dart';

/// An extension on [AnsiPen] that adds more methods for setting colors
extension AnsiPenColorsExtension on AnsiPen {
  void orange() => rgb(
        r: 255.toDouble() / 255,
        g: 133.toDouble() / 255,
        b: 61.toDouble() / 255,
      );
}
