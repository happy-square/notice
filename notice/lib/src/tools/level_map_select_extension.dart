import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:notice/src/notice_level.dart';

extension LevelMapSelectExtension<T> on Map<NoticeLevel, T> {
  T getByEqualLevelOrHigher(NoticeLevel level) => entries
      .toIList()
      .sort((a, b) => a.key.priority - b.key.priority)
      .firstWhere((entry) => entry.key.priority >= level.priority)
      .value;
}
