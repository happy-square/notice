// https://en.wikipedia.org/wiki/Box-drawing_character
class UnicodeBorderStyle {
  const UnicodeBorderStyle({
    required this.vertical,
    required this.horizontal,
    required this.upAndLeft,
    required this.upAndRight,
    required this.downAndLeft,
    required this.downAndRight,
    required this.verticalAndLeft,
    required this.verticalAndRight,
  });

  final String vertical;
  final String horizontal;
  final String upAndLeft;
  final String upAndRight;
  final String downAndLeft;
  final String downAndRight;
  final String verticalAndLeft;
  final String verticalAndRight;

  static const heavy = UnicodeBorderStyle(
      vertical: '┃',
      horizontal: '━',
      upAndLeft: '┛',
      upAndRight: '┗',
      downAndLeft: '┓',
      downAndRight: '┏',
      verticalAndLeft: '┫',
      verticalAndRight: '┣');

  static const light = UnicodeBorderStyle(
      vertical: '│',
      horizontal: '─',
      upAndLeft: '┘',
      upAndRight: '└',
      downAndLeft: '┐',
      downAndRight: '┌',
      verticalAndLeft: '┤',
      verticalAndRight: '├');
}
