import 'dart:io';

import 'package:notice/src/notice_writer.dart';
import 'package:notice/src/notice_writer_context.dart';

/// A [NoticeWriter] that writes to the console.
class ConsoleWriter implements NoticeWriter {
  /// Creates a new [ConsoleWriter].
  const ConsoleWriter();

  @override
  void write(String string) {
    print(string);
  }

  @override
  NoticeWriterContext context() => NoticeWriterContext(
        colorSupport: stdioType(stdout) == StdioType.terminal,
      );
}
