import 'dart:io';

import 'package:notice/src/notice_writer.dart';
import 'package:notice/src/notice_writer_context.dart';

/// A [NoticeWriter] that writes to a file.
class FileWriter implements NoticeWriter {
  /// Creates a new [FileWriter].
  const FileWriter(this.file);

  /// The file to write to.
  final File file;

  @override
  void write(String string) {
    // this could potentially be optimized by using a buffer
    // we would need to make sure to flush the buffer on close though
    file.writeAsStringSync(string, mode: FileMode.append);
  }

  @override
  NoticeWriterContext context() => const NoticeWriterContext(
        colorSupport: false,
      );
}
