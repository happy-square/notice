library;

/// exposes the version of the package so that it can be used
/// as additional logging information
const version = "2.0.2";
