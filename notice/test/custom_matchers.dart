import 'package:test/expect.dart';

Matcher singleElement(Object object) => predicate((value) {
      value as Iterable;
      return value.single == object;
    }, "single element equals $object");
