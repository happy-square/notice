import 'package:notice/notice.dart';
import 'package:notice/registry.dart';
import 'package:test/test.dart';

import 'custom_matchers.dart';
import 'history.dart';
import 'test_helpers.dart';

main() {
  group("end to end", () {
    test("simplest case", () {
      final history = History();
      final notice = Notice(outputs: [history]);
      final record = createTestRecord();
      notice.onRecord(record);
      expect(history.records, singleElement(record));
    });

    test("nested notices", () {
      final history = History();
      final parentNotice = Notice(outputs: [history]);
      final childNotice = Notice.childOf(parentNotice);
      final record = createTestRecord();
      childNotice.onRecord(record);
      expect(history.records, singleElement(record));
    });

    test("via registry", () {
      final registry = NoticeRegistry();
      final history = History();
      Notice(registries: [registry], outputs: [history]);
      final registeredNotice = Notice.childOf(registry);
      final record = createTestRecord();
      registeredNotice.onRecord(record);
      expect(history.records, singleElement(record));
    });

    test("breadcrumbs", () {
      final history = History();
      final notice = Notice(outputs: [history]);
      const childBreadcrumb = "child";
      final childNotice = Notice.childOf(notice, breadcrumb: childBreadcrumb);
      const grandchildBreadcrumb = "grandchild";
      final grandchildNotice =
          Notice.childOf(childNotice, breadcrumb: grandchildBreadcrumb);
      final record = createTestRecord();
      grandchildNotice.onRecord(record);
      final expectedRecord = record
          .copyWithBreadcrumb(grandchildBreadcrumb)
          .copyWithBreadcrumb(childBreadcrumb);
      expect(history.records, singleElement(expectedRecord));
    });
  });
}
