import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:notice/output.dart';
import 'package:notice/src/notice_logger.dart';

class History with NoticeLogger implements NoticeOutput {
  final _records = List<NoticeRecord>.empty(growable: true);

  @override
  void onRecord(NoticeRecord record) {
    _records.add(record);
  }

  IList<NoticeRecord> get records => _records.toIList();

  void reset() => _records.clear();
}
