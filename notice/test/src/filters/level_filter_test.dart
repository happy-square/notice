import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:notice/filters.dart';
import 'package:notice/foundation.dart';
import 'package:test/test.dart';

import '../../test_helpers.dart';

main() {
  group("LevelFilter tests", () {
    test("Records with equal levels should be passed", () {
      final levelFilter = LevelFilter(NoticeLevel.info);
      final record = _createRecordWithLevel(NoticeLevel.info);
      expect(levelFilter.filter(record), isTrue);
    });

    test("Records with higher priority levels should be passed", () {
      final levelFilter = LevelFilter(NoticeLevel.info);
      final record = _createRecordWithLevel(NoticeLevel.warn);
      expect(levelFilter.filter(record), isTrue);
    });

    test("Records with lower priority levels should not be passed", () {
      final levelFilter = LevelFilter(NoticeLevel.info);
      final record = _createRecordWithLevel(NoticeLevel.trace);
      expect(levelFilter.filter(record), isFalse);
    });
  });
}

NoticeRecord _createRecordWithLevel(NoticeLevel level) => NoticeRecord(
      level: level,
      message: "this is a message",
      error: null,
      stackTrace: StackTrace.empty,
      timestamp: testTimestamp,
      breadcrumbs: IList(),
    );
