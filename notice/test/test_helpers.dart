import 'package:fast_immutable_collections/fast_immutable_collections.dart';
import 'package:notice/notice.dart';
import 'package:notice/src/notice_record.dart';

final testTimestamp = DateTime.utc(2020, 1, 1, 12, 0, 0, 0);

NoticeRecord createTestRecord() => NoticeRecord(
      level: NoticeLevel.info,
      message: "test message",
      error: "test error",
      stackTrace: StackTrace.empty,
      timestamp: testTimestamp,
      breadcrumbs: IList(),
    );
