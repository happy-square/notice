import 'dart:io';

import 'package:notice/version.dart';
import 'package:pubspec_parse/pubspec_parse.dart';
import 'package:test/test.dart';

main() {
  test(
      "the version of the package should be the same as the version const it exposes",
      () {
    expect(version, equals(_readPubspec().version.toString()));
  });
}

Pubspec _readPubspec() {
  return Pubspec.parse(File("pubspec.yaml").readAsStringSync());
}
