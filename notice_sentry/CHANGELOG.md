## 2.0.1
- Changed some docs
- Added warning log, when trying to output to the integration but Sentry has not been registerd.

## 2.0.0
- Updated notice dependency to ^2.0.0

## 1.2.0
- updated notice to ^1.2.0

## 1.1.0
- Initial version.
