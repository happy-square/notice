import 'dart:async';
import 'package:notice/notice.dart';
import 'package:notice_sentry/notice_sentry.dart';
import 'package:sentry/sentry.dart';

final _noticeIntegration = NoticeIntegration();

final mainNotice = Notice(breadcrumb: 'Example', outputs: [_noticeIntegration]);

Future<void> main() async {
  // ATTENTION: Change the DSN below with your own to see the events in Sentry. Get one at sentry.io
  const dsn =
      'https://e85b375ffb9f43cf8bdf9787768149e0@o447951.ingest.sentry.io/5428562';

  await Sentry.init(
    (options) {
      options.dsn = dsn;
      options.addIntegration(_noticeIntegration);
    },
    appRunner: runApp,
  );
}

Future<void> runApp() async {
  final notice = Notice.childOf(mainNotice, breadcrumb: 'runApp');

  notice.warn('this is a warning!');

  try {
    throw Exception();
  } catch (error, stackTrace) {
    // The log from above will be contained in this crash report.
    await Sentry.captureException(
      error,
      stackTrace: stackTrace,
    );
  }
}
