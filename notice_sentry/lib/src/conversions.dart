import 'package:notice/foundation.dart';
import 'package:sentry/sentry.dart';

extension LogRecordConversionExtension on NoticeRecord {
  Breadcrumb toBreadcrumb() {
    return Breadcrumb(
      category: 'log',
      type: 'debug',
      timestamp: timestamp.toUtc(),
      level: level.toSentryLevel(),
      message: message.toString(),
      data: <String, Object>{
        //if (object != null) 'NoticeRecord.object': object!,
        if (error != null) 'NoticeRecord.error': error!,
        'NoticeRecord.stackTrace': stackTrace,
        'NoticeRecord.breadcrumbs': breadcrumbs.join('.'),
      },
    );
  }

  SentryEvent toEvent() {
    return SentryEvent(
      timestamp: timestamp.toUtc(),
      logger: breadcrumbs.join('.'),
      level: level.toSentryLevel(),
      message: SentryMessage(message.toString()),
      throwable: error,
    );
  }
}

extension LogLevelConversionExtension on NoticeLevel {
  SentryLevel toSentryLevel() {
    if (priority >= NoticeLevel.fatal.priority) {
      return SentryLevel.fatal;
    } else if (priority >= NoticeLevel.error.priority) {
      return SentryLevel.error;
    } else if (priority >= NoticeLevel.warn.priority) {
      return SentryLevel.warning;
    } else if (priority >= NoticeLevel.info.priority) {
      return SentryLevel.info;
    } else {
      return SentryLevel.debug;
    }
  }
}
