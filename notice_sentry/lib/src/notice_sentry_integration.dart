import 'dart:async';

import 'package:notice/filter.dart';
import 'package:notice/filters.dart';
import 'package:notice/notice.dart';
import 'package:notice/output.dart';
import 'package:notice/registry.dart';
import 'package:notice/version.dart';
import 'package:notice_sentry/src/conversions.dart';
import 'package:sentry/sentry.dart';

const _defaultMinBreadcrumbLevel = NoticeLevel.info;
const _defaultMinEventLevel = NoticeLevel.error;

final _notice =
    Notice.childOf(globalNoticeRegistry, breadcrumb: "NoticeIntegration");

/// A Sentry [Integration] and [NoticeOutput] that sends all [NoticeRecord]s
/// to Sentry. Wether a [NoticeRecord] is sent as a [Breadcrumb], a
/// [SentryEvent] or not at all is determined by the [NoticeFilter]s
/// given to the constructor.
class NoticeIntegration implements Integration<SentryOptions>, NoticeOutput {
  /// Creates the [NoticeIntegration].
  ///
  /// All log events equal or higher than [minBreadcrumbLevel] or
  /// filtered by [breadcrumbFilter] are recorded as a [Breadcrumb].
  /// All log events equal or higher than [minEventLevel] or
  /// filtered by [eventFilter] are recorded as a [SentryEvent].
  NoticeIntegration({
    NoticeLevel? minBreadcrumbLevel,
    NoticeLevel? minEventLevel,
    NoticeFilter? breadcrumbFilter,
    NoticeFilter? eventFilter,
  })  : assert(minBreadcrumbLevel == null || breadcrumbFilter == null,
            'Only one of minBreadcrumbLevel or breadcrumbFilter can be set.'),
        assert(minEventLevel == null || eventFilter == null,
            'Only one of minEventLevel or eventFilter can be set.'),
        _breadcrumbFilter = breadcrumbFilter ??
            LevelFilter(minBreadcrumbLevel ?? _defaultMinBreadcrumbLevel),
        _eventFilter =
            eventFilter ?? LevelFilter(minEventLevel ?? _defaultMinEventLevel);

  final NoticeFilter _breadcrumbFilter;
  final NoticeFilter _eventFilter;
  Hub? _hub;

  @override
  void call(Hub hub, SentryOptions options) {
    _hub = hub;
    options.sdk.addPackage("notice", version);
    options.sdk.addIntegration('NoticeIntegration');
  }

  @override
  Future<void> close() async {
    _hub = null;
  }

  Future<void> _dispatch(NoticeRecord record) async {
    if (_hub == null) {
      // As we are logging into the global registry, the log we sent off might
      // end up here again, creating a loop. To avoid this, we check if the
      // breadcrumb is equal to our own and do not log in that case.
      // This is not a problem of this integration, but of the registry
      // itself. It should be fixed there at some point in the future.
      // TODO: Remove this workaround once we have a solution.
      if (record.breadcrumbs.last != _notice.breadcrumb) {
        _notice.warn("NoticeRecord could not be sent to Sentry because "
            "NoticeIntegration is not registered.");
      }
      return;
    }

    // The event must be logged first, otherwise the log would also be added
    // to the breadcrumbs for itself.
    if (_eventFilter.filter(record)) {
      await _hub!.captureEvent(
        record.toEvent(),
        stackTrace: record.stackTrace,
        hint: Hint.withMap({TypeCheckHint.record: record}),
      );
    }

    if (_breadcrumbFilter.filter(record)) {
      await _hub!.addBreadcrumb(
        record.toBreadcrumb(),
        hint: Hint.withMap({TypeCheckHint.record: record}),
      );
    }
  }

  @override
  void onRecord(NoticeRecord record) {
    unawaited(_dispatch(record));
  }
}
