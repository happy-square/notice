import 'dart:io';

main() {
  final projects = {
    Uri.parse('notice/'): [UpdateReadmeTask()],
  };
  for (final project in projects.entries) {
    for (final task in project.value) {
      task.run(project.key);
    }
  }
}

abstract interface class Task {
  void run(Uri project);
}

class UpdateReadmeTask implements Task {
  static final mainCodeRegex = RegExp(r'main\(\) {\n([^}]*)\n}');

  @override
  void run(Uri project) {
    final declarations = detectDeclarations(project);
    final results = declarations
        .map((declaration) => runExample(project, declaration))
        .toList();
    final template = File.fromUri(project.resolve('./README.template.md'))
      .readAsStringSync();
    var readmeText = template;
    for (final result in results) {
      var replacement = '```dart\n${result.code}\n```';
      if (result.output != null) {
        final output = result.output!
            .split("\n")
            .map((o) => '`$o`')
            .join("\\\n");
        replacement += '\nprints:\\\n$output';
      }
      readmeText = readmeText.replaceAll(result.declaration.raw, replacement);
    }
    File.fromUri(project.resolve('./README.md')).writeAsStringSync(readmeText);
  }

  static ReadmeExampleResult runExample(
      Uri project, ReadmeExampleDeclaration declaration) {
    final path = project.resolve('example/${declaration.name}.dart');
    //final path = File.fromUri(relativePath).absolute.uri;
    final text = File.fromUri(path).readAsStringSync();
    final code = mainCodeRegex.firstMatch(text)!.group(1)!
        .split('\n')
        .map((line) => line.replaceFirst('  ', ''))
        .join('\n');
    if (!declaration.output) {
      return ReadmeExampleResult(
        declaration: declaration,
        code: code,
        output: null,
      );
    }
    final output = (Process.runSync(
      "dart", ["run", path.toFilePath()], stdoutEncoding: systemEncoding
    ).stdout as String).trim();
    return ReadmeExampleResult(
      declaration: declaration,
      code: code,
      output: output
    );
  }

  static List<ReadmeExampleDeclaration> detectDeclarations(Uri project) {
    final readme = File.fromUri(project.resolve('README.template.md'));
    final lines = readme.readAsLinesSync();
    final declarations = lines
        .map((line) => ReadmeExampleDeclaration.parse(line))
        .whereType<ReadmeExampleDeclaration>()
        .toList();
    return declarations;
  }
}

class ReadmeExampleResult {
  const ReadmeExampleResult({
    required this.declaration,
    required this.code,
    required this.output,
  });

  final ReadmeExampleDeclaration declaration;
  final String code;
  final String? output;
}

class ReadmeExampleDeclaration {
  static final regex
      = RegExp(r"::\[EXAMPLE; name: '([^']+)', output: (true|false)\]");

  const ReadmeExampleDeclaration({
    required this.raw,
    required this.name,
    required this.output,
  });

  static ReadmeExampleDeclaration? parse(String line) {
    final match = regex.firstMatch(line);
    if (match == null) return null;
    return ReadmeExampleDeclaration(
      raw: match.group(0)!,
      name: match.group(1)!,
      output: match.group(2) == 'true',
    );
  }

  final String raw;
  final String name;
  final bool output;
}
